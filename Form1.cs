﻿using RpgCharacterGeneratorStructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CsvHelper;
using System.Data.SqlClient;

namespace RPG_GUI
{
    public partial class charcterSheet : Form

    {
        public static List<Character> allCharacters = new List<Character>();
        

        //Sets initial properties for buttons to show/hide
        public charcterSheet()
        {
            InitializeComponent();
            createCharButton.Show();
            editDeleteButton.Show();
            welcomeLabel.Show();
            saveButton.Hide();
            characterNameInput.Hide();
            characterAgeInput.Hide();
            button2.Hide();
            characterSheetListbox.Hide();
            comboBoxClass.Hide();
            comboBoxGender.Hide();
            saveButton.Hide();
            label2.Hide();
            label3.Hide();
            label4.Hide();
            label5.Hide();
            label6.Hide();
            label7.Hide();
            label8.Hide();
            newNameBox.Hide();
            editdeleteBox.Hide();
            deleteButton.Hide();
            editButton.Hide();
            comboBoxGender.Items.Add(CharacterGender.Female);
            comboBoxGender.Items.Add(CharacterGender.Male);
            comboBoxGender.Items.Add(CharacterGender.None);
        }

        #region navigation buttons
        private void createCharButton_Click(object sender, EventArgs e)
        {
            ShowCreate();
        }
        private void editDeleteButton_Click(object sender, EventArgs e)
        {
            ReadCharacter();
            ShowEditDelete();
        }
        //The create button navigates to the create page
        public void ShowCreate()
        {
            characterNameInput.Show();
            characterAgeInput.Show();
            button2.Show();
            characterSheetListbox.Show();
            comboBoxClass.Show();
            comboBoxGender.Show();
            saveButton.Show();
            label2.Show();
            label3.Show();
            label4.Show();
            label5.Show();
            label6.Show();
            label7.Show();

            createCharButton.Hide();
            editDeleteButton.Hide();
            welcomeLabel.Hide();

        } 
        //Edit/Delete button navigates to the edit delete page
        public void ShowEditDelete()
        {
            createCharButton.Hide();
            editDeleteButton.Hide();
            welcomeLabel.Hide();
            editdeleteBox.Show();
            deleteButton.Show();
            editButton.Show();
            label8.Show();
            newNameBox.Show();
        }
        #endregion
        //Reads all characters from SQL server
        public void ReadCharacter()
        {
            try
            {
                SqlConnectionStringBuilder buildConnection = new SqlConnectionStringBuilder();
                buildConnection.DataSource = "PC7362\\SQLEXPRESS";
                buildConnection.InitialCatalog = "RPG_Characters";
                buildConnection.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(buildConnection.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * " +
                        "FROM dbo.Characters";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            editdeleteBox.Items.Add($"{reader.GetName(1)}: {reader.GetName(2)}");
                            editdeleteBox.Items.Add("------------------------------");
                            while (reader.Read())
                            {
                                editdeleteBox.Items.Add($"{reader.GetString(1)}: {reader.GetString(2)}");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //Method for saving created character to the SQL server
        public void SaveCharacter()
        {
            try
            {
                SqlConnectionStringBuilder buildConnection = new SqlConnectionStringBuilder();
                buildConnection.DataSource = "PC7362\\SQLEXPRESS";
                buildConnection.InitialCatalog = "RPG_Characters";
                buildConnection.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(buildConnection.ConnectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO Characters(Name,Class,MaxHp,Hp,State,MaxWakeUpAttempts,WakeUpAttempts,WakeUpChance," +
                    "Xp,Gender, " +
                    "SexChangeRisk,MaxWeapons,Armour) VALUES (@Name,@Class,@MaxHp,@MaxHp,@State,@MaxWakeUpAttempts," +
                    "@WakeUpAttempts,@WakeUpChance,@Xp,@Gender, " +
                    "@SexChangeRisk,@MaxWeapons,@Armour)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Name", allCharacters[allCharacters.Count() - 1].Name);
                        command.Parameters.AddWithValue("@Class", comboBoxClass.Text);
                        command.Parameters.AddWithValue("@MaxHp", allCharacters[allCharacters.Count() - 1].MaxHp);
                        command.Parameters.AddWithValue("@Hp", allCharacters[allCharacters.Count() - 1].Hp);
                        command.Parameters.AddWithValue("@State", allCharacters[allCharacters.Count() - 1].State);
                        command.Parameters.AddWithValue("@MaxWakeUpAttempts", allCharacters[allCharacters.Count() - 1].MaxWakeUpAttempts);
                        command.Parameters.AddWithValue("@WakeUpAttempts", allCharacters[allCharacters.Count() - 1].WakeUpAttempts);
                        command.Parameters.AddWithValue("@WakeUpChance", allCharacters[allCharacters.Count() - 1].WakeUpChance);
                        command.Parameters.AddWithValue("@Xp", allCharacters[allCharacters.Count() - 1].Xp);
                        command.Parameters.AddWithValue("@Gender", allCharacters[allCharacters.Count() - 1].Gender);
                        command.Parameters.AddWithValue("@SexChangeRisk", allCharacters[allCharacters.Count() - 1].SexChangeRisk);
                        command.Parameters.AddWithValue("@MaxWeapons", allCharacters[allCharacters.Count() - 1].MaxWeapons);
                        command.Parameters.AddWithValue("@Armour", allCharacters[allCharacters.Count() - 1].Armour);


                        command.ExecuteNonQuery();
                    }
                }
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Method that displays created character to the listbox
        public void button2_Click(object sender, EventArgs e)
        {
            string characterName;
            int hp;
            CharacterGender gender;
            string classChosen;

            characterName = characterNameInput.Text;
            hp = Convert.ToInt32(characterAgeInput.Text);
            gender = (CharacterGender)comboBoxGender.SelectedItem;
            classChosen = comboBoxClass.Text;

            switch (classChosen)
            {
                case "Berserker":
                    allCharacters.Add(new Berserker(characterName, hp, gender));
                    break;
                case "Knight":
                    allCharacters.Add(new Knight(characterName, hp, gender));
                    break;
                case "Battlemage":
                    allCharacters.Add(new BattleMage(characterName, hp, gender));
                    break;
                case "Cleric":
                    allCharacters.Add(new Cleric(characterName, hp, gender));
                    break;
                case "Skeleton":
                    allCharacters.Add(new Skeleton(characterName, hp, gender));
                    break;
                case "Zombie":
                    allCharacters.Add(new Zombie(characterName, hp, gender));
                    break;
            }

            Character newestCharacter = allCharacters[allCharacters.Count() - 1];
            characterSheetListbox.Items.Add($"Name: {newestCharacter.Name}");
            characterSheetListbox.Items.Add($"Gender: {newestCharacter.Gender}");
            characterSheetListbox.Items.Add($"Class: {classChosen}");
            characterSheetListbox.Items.Add($"Hp: {newestCharacter.Hp}");
            characterSheetListbox.Items.Add($"Armour: {newestCharacter.Armour}");
            characterSheetListbox.Items.Add($"Xp: {newestCharacter.Xp}");
            characterSheetListbox.Items.Add($"State: {newestCharacter.State}");
            characterSheetListbox.Items.Add($"Sex Change Risk: {newestCharacter.SexChangeRisk}%");
            characterSheetListbox.Items.Add($"Weapons:");
            foreach (Weapon weapon in (newestCharacter.Weapons))
            {
                characterSheetListbox.Items.Add(weapon);
            };
            saveButton.Show();

        }

        //Method that uses savacharacter() and clears input, so new character can be created
        public void saveButton_Click(object sender, EventArgs e)
        {
            SaveCharacter();
            MessageBox.Show("Character Saved!");
            characterSheetListbox.Items.Clear();
            characterNameInput.Clear();
            characterAgeInput.Clear();
            saveButton.Hide();
        }

        #region unused items
        private void comboBoxGender_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void outputCharacterSheet_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public void characterNameInput_TextChanged(object sender, EventArgs e)
        {

        }
        #endregion
        //Deletes character chosen from the SQL server
        public void deleteCharacter(string name)
        {
            try
            {
                SqlConnectionStringBuilder buildConnection = new SqlConnectionStringBuilder();
                buildConnection.DataSource = "PC7362\\SQLEXPRESS";
                buildConnection.InitialCatalog = "RPG_Characters";
                buildConnection.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(buildConnection.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM dbo.Characters WHERE Name=@name";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //Uses deletecharacter() and refreshes listbox
        private void deleteButton_Click(object sender, EventArgs e)
        {
            string text = editdeleteBox.GetItemText(editdeleteBox.SelectedItem);
            int firstSpace = text.IndexOf(":");
            MessageBox.Show(text.Substring(0, firstSpace));

            deleteCharacter(text.Substring(0, firstSpace));
            editdeleteBox.Items.Clear();
            ReadCharacter();
        }
        //Gives chosen character the new name 
        public void editCharacter(string oldName, string newName)
        {
            try
            {
                SqlConnectionStringBuilder buildConnection = new SqlConnectionStringBuilder();
                buildConnection.DataSource = "PC7362\\SQLEXPRESS";
                buildConnection.InitialCatalog = "RPG_Characters";
                buildConnection.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(buildConnection.ConnectionString))
                {
                    connection.Open();
                    string sql = "UPDATE dbo.Characters SET Name = @newName WHERE Name = @oldName";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", newName);
                        command.Parameters.AddWithValue("@oldName", oldName);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //Uses editCharacter() and refreshes listbox
        private void editButton_Click(object sender, EventArgs e)
        {
            string text = editdeleteBox.GetItemText(editdeleteBox.SelectedItem);
            int firstSpace = text.IndexOf(":");

            MessageBox.Show($"Old name: {text.Substring(0, firstSpace)}\nNew name: {newNameBox.Text}");

            editCharacter(text.Substring(0, firstSpace), newNameBox.Text);

            editdeleteBox.Items.Clear();
            ReadCharacter();
        }
    }
        
    }
