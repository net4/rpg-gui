﻿namespace RPG_GUI
{
    partial class charcterSheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(charcterSheet));
            this.characterNameInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.characterAgeInput = new System.Windows.Forms.TextBox();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.comboBoxClass = new System.Windows.Forms.ComboBox();
            this.characterSheetListbox = new System.Windows.Forms.ListBox();
            this.copyright = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.createCharButton = new System.Windows.Forms.Button();
            this.editDeleteButton = new System.Windows.Forms.Button();
            this.welcomeLabel = new System.Windows.Forms.Label();
            this.editdeleteBox = new System.Windows.Forms.ListBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.newNameBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // characterNameInput
            // 
            this.characterNameInput.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.characterNameInput.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.characterNameInput.Location = new System.Drawing.Point(249, 176);
            this.characterNameInput.Multiline = true;
            this.characterNameInput.Name = "characterNameInput";
            this.characterNameInput.Size = new System.Drawing.Size(260, 28);
            this.characterNameInput.TabIndex = 0;
            this.characterNameInput.TextChanged += new System.EventHandler(this.characterNameInput_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 12);
            this.label1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(104, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(412, 66);
            this.label2.TabIndex = 2;
            this.label2.Text = "Create a character!";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(104, 456);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(405, 67);
            this.button2.TabIndex = 4;
            this.button2.Text = "Create";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(665, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(384, 66);
            this.label3.TabIndex = 5;
            this.label3.Text = "Character Sheet";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(104, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 37);
            this.label4.TabIndex = 6;
            this.label4.Text = "Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(104, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 37);
            this.label5.TabIndex = 7;
            this.label5.Text = "HP:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(104, 302);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 37);
            this.label6.TabIndex = 8;
            this.label6.Text = "Gender:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(105, 372);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 37);
            this.label7.TabIndex = 9;
            this.label7.Text = "Class:";
            // 
            // characterAgeInput
            // 
            this.characterAgeInput.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold);
            this.characterAgeInput.Location = new System.Drawing.Point(358, 232);
            this.characterAgeInput.Multiline = true;
            this.characterAgeInput.Name = "characterAgeInput";
            this.characterAgeInput.Size = new System.Drawing.Size(151, 28);
            this.characterAgeInput.TabIndex = 10;
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Location = new System.Drawing.Point(358, 302);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(151, 25);
            this.comboBoxGender.TabIndex = 13;
            this.comboBoxGender.SelectedIndexChanged += new System.EventHandler(this.comboBoxGender_SelectedIndexChanged);
            // 
            // comboBoxClass
            // 
            this.comboBoxClass.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxClass.FormattingEnabled = true;
            this.comboBoxClass.Items.AddRange(new object[] {
            "Berserker",
            "Knight",
            "Battlemage",
            "Cleric",
            "Skeleton",
            "Zombie"});
            this.comboBoxClass.Location = new System.Drawing.Point(358, 372);
            this.comboBoxClass.Name = "comboBoxClass";
            this.comboBoxClass.Size = new System.Drawing.Size(150, 25);
            this.comboBoxClass.TabIndex = 14;
            // 
            // characterSheetListbox
            // 
            this.characterSheetListbox.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.characterSheetListbox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.characterSheetListbox.FormattingEnabled = true;
            this.characterSheetListbox.ItemHeight = 28;
            this.characterSheetListbox.Location = new System.Drawing.Point(673, 176);
            this.characterSheetListbox.Name = "characterSheetListbox";
            this.characterSheetListbox.Size = new System.Drawing.Size(376, 256);
            this.characterSheetListbox.TabIndex = 15;
            // 
            // copyright
            // 
            this.copyright.AutoSize = true;
            this.copyright.BackColor = System.Drawing.Color.Transparent;
            this.copyright.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyright.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.copyright.Location = new System.Drawing.Point(943, 626);
            this.copyright.Name = "copyright";
            this.copyright.Size = new System.Drawing.Size(196, 16);
            this.copyright.TabIndex = 16;
            this.copyright.Text = "Copyright Bjørke-Hansen Consultings";
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.saveButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.saveButton.Location = new System.Drawing.Point(673, 456);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(376, 67);
            this.saveButton.TabIndex = 17;
            this.saveButton.Text = "Save Character";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // createCharButton
            // 
            this.createCharButton.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.createCharButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.createCharButton.Location = new System.Drawing.Point(205, 249);
            this.createCharButton.Name = "createCharButton";
            this.createCharButton.Size = new System.Drawing.Size(304, 160);
            this.createCharButton.TabIndex = 18;
            this.createCharButton.Text = "Create New!";
            this.createCharButton.UseVisualStyleBackColor = true;
            this.createCharButton.Click += new System.EventHandler(this.createCharButton_Click);
            // 
            // editDeleteButton
            // 
            this.editDeleteButton.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.editDeleteButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.editDeleteButton.Location = new System.Drawing.Point(673, 246);
            this.editDeleteButton.Name = "editDeleteButton";
            this.editDeleteButton.Size = new System.Drawing.Size(304, 160);
            this.editDeleteButton.TabIndex = 19;
            this.editDeleteButton.Text = "Edit / Delete";
            this.editDeleteButton.UseVisualStyleBackColor = true;
            this.editDeleteButton.Click += new System.EventHandler(this.editDeleteButton_Click);
            // 
            // welcomeLabel
            // 
            this.welcomeLabel.Font = new System.Drawing.Font("Palatino Linotype", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcomeLabel.Location = new System.Drawing.Point(398, 61);
            this.welcomeLabel.Name = "welcomeLabel";
            this.welcomeLabel.Size = new System.Drawing.Size(386, 120);
            this.welcomeLabel.TabIndex = 20;
            this.welcomeLabel.Text = "Welcome!";
            // 
            // editdeleteBox
            // 
            this.editdeleteBox.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold);
            this.editdeleteBox.FormattingEnabled = true;
            this.editdeleteBox.ItemHeight = 28;
            this.editdeleteBox.Location = new System.Drawing.Point(622, 176);
            this.editdeleteBox.Name = "editdeleteBox";
            this.editdeleteBox.Size = new System.Drawing.Size(427, 284);
            this.editdeleteBox.TabIndex = 21;
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.deleteButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.deleteButton.Location = new System.Drawing.Point(372, 384);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(174, 76);
            this.deleteButton.TabIndex = 22;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // editButton
            // 
            this.editButton.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Bold);
            this.editButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.editButton.Location = new System.Drawing.Point(112, 384);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(174, 76);
            this.editButton.TabIndex = 23;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // newNameBox
            // 
            this.newNameBox.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold);
            this.newNameBox.Location = new System.Drawing.Point(308, 246);
            this.newNameBox.Multiline = true;
            this.newNameBox.Name = "newNameBox";
            this.newNameBox.Size = new System.Drawing.Size(238, 28);
            this.newNameBox.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(104, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 44);
            this.label8.TabIndex = 25;
            this.label8.Text = "New name:";
            // 
            // charcterSheet
            // 
            this.AccessibleName = "";
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1151, 651);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.newNameBox);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.editdeleteBox);
            this.Controls.Add(this.welcomeLabel);
            this.Controls.Add(this.editDeleteButton);
            this.Controls.Add(this.createCharButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.copyright);
            this.Controls.Add(this.characterSheetListbox);
            this.Controls.Add(this.comboBoxClass);
            this.Controls.Add(this.comboBoxGender);
            this.Controls.Add(this.characterAgeInput);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.characterNameInput);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Algerian", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "charcterSheet";
            this.Text = "RPG Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox characterNameInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox characterAgeInput;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.ComboBox comboBoxClass;
        private System.Windows.Forms.ListBox characterSheetListbox;
        private System.Windows.Forms.Label copyright;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button createCharButton;
        private System.Windows.Forms.Button editDeleteButton;
        private System.Windows.Forms.Label welcomeLabel;
        private System.Windows.Forms.ListBox editdeleteBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.TextBox newNameBox;
        private System.Windows.Forms.Label label8;
    }
}

